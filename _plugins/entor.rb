module EntryPageGenerator
  class Generator < Jekyll::Generator
    def generate(site)
      entries = []
      site.data.each do |letter, data|
        data.each do |entry|
          entries << {
            "url" => "/w/#{entry['word']}.html",
            "layout" => "entry",
            "title" => entry['word'],
            "part_of_speech" => entry['part_of_speech'],
            "definition" => entry['definition'],
            "example_sentence" => entry['example_sentence']
          }
        end
      end

      entries.each do |entry|
        site.pages << EntryPage.new(site, site.source, entry)
      end
    end
  end

  class EntryPage < Jekyll::Page
    def initialize(site, base, entry)
      @site = site
      @base = base
      @dir = ""
      @name = entry['url']

      self.process(@name)
      self.read_yaml(File.join(base, "_layouts"), entry['layout'] + ".html")
      self.data.merge!(entry)
    end
  end
end
